use ructe::Ructe;

fn main() -> Result<(), anyhow::Error> {
    dotenv::dotenv().ok();
    let mut ructe = Ructe::from_env()?;
    let mut statics = ructe.statics()?;
    statics.add_sass_file("scss/layout.scss")?;
    statics.add_sass_file("scss/images.scss")?;
    statics.add_sass_file("scss/form.scss")?;
    statics.add_sass_file("scss/not_found.scss")?;
    statics.add_files("static")?;
    ructe.compile_templates("templates")?;

    Ok(())
}
