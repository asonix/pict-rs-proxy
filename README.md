# pict-rs-proxy
_a demo frontend for pict-rs supporting noscript_

![Index page](https://git.asonix.dog/asonix/pict-rs-proxy/raw/branch/main/images/index.png)
![Upload page](https://git.asonix.dog/asonix/pict-rs-proxy/raw/branch/main/images/upload.png)

## Usage
### Running
```
$ pict-rs-proxy -h
A simple web frontend for pict-rs

Usage: pict-rs-proxy [OPTIONS]

Options:
  -a, --addr <ADDR>
          The address and port the server binds to [env: PICTRS_PROXY_ADDR=] [default: 0.0.0.0:8081]
  -u, --upstream <UPSTREAM>
          The url of the upstream pict-rs server [env: PICTRS_PROXY_UPSTREAM=] [default: http://localhost:8080]
  -d, --domain <DOMAIN>
          The scheme, domain, and optional port of the pict-rs proxy server [env: PICTRS_PROXY_DOMAIN=] [default: http://localhost:8081]
      --console-event-buffer-size <CONSOLE_EVENT_BUFFER_SIZE>
          Number of events to buffer for the console subscriber. When unset, console will be disabled [env: PICTRS_PROXY_CONSOLE_BUFFER_SIZE=]
  -o, --opentelemetry-url <OPENTELEMETRY_URL>
          URL of OpenTelemetry Collector [env: PICTRS_PROXY_OPENTELEMETRY_URL=]
  -h, --help
          Print help information
  -V, --version
          Print version information
```

#### Examples
Running on all interfaces, port 8081, proxying to localhost:8080
```
$ ./pict-rs-proxy
```
Running locally, port 9000, proxying to localhost:4000
```
$ ./pict-rs-proxy -a 127.0.0.1:9000 -d localhost:4000 -u localhost:4000
```

#### Docker production
```
# Create a folder for the files (anywhere works)
mkdir ./pict-rs-proxy
cd ./pict-rs-proxy
mkdir -p volumes/pictrs
sudo chown -R 991:991 volumes/pictrs
wget https://git.asonix.dog/asonix/pict-rs-proxy/raw/branch/main/docker/prod/docker-compose.yml
sudo docker-compose up -d
```

#### Docker development
```
git clone https://git.asonix.dog/asonix/pict-rs-proxy
cd pict-rs-proxy/docker/dev
docker-compose up --build
```

## Contributing
Feel free to open issues for anything you find an issue with. Please note that any contributed code will be licensed under the AGPLv3.

## License

Copyright © 2021 Riley Trautman

pict-rs-proxy is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

pict-rs-proxy is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. This file is part of pict-rs.

You should have received a copy of the GNU General Public License along with pict-rs. If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).
