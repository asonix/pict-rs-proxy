(function() {
  function reloadOnComplete() {
    if (document.readyState === 'complete') {
      // Do not use window.reload() because it's bugged on firefox
      window.location.href = window.location.href;
    }
  }

  document.addEventListener('readystatechange', reloadOnComplete);
  reloadOnComplete();
})()
