(function(fn) {
    if (document.readyState === "complete" || document.readyState === "interactive") {
        setTimeout(fn, 1);
    } else {
        document.addEventListener("DOMContentLoaded", fn);
    }
})(function() {
    var text = document.getElementById("file-upload-text");
    var input = document.getElementById("file-upload-input");
    if (!text || !input) {
        console.err("Error fetching file upload");
        return;
    }

    input.addEventListener("change", function(event) {
        if (event.target.files) {
            text.textContent = event.target.files.length + " Selected File(s)";
        }
    });
});
